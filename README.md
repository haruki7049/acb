# Absructed-CodeBase

The Absructed-CodeBase(acb) can abstruct your code, and manage the abstructed code as Git.

from c-lang:
```c
#include <stdio.h>

int main()
{
  printf("hello\n");
  printf("foo!!\n");
  return 0;
}
```

to:
```toml
include = "stdio.h"

[main.func]
type = "int"
entrypoint = true
defMode = true
[main.func.expressions.0]
name = "printf"
args = [ "hello\n" ]
[main.func.expressions.1]
name = "printf"
args = [ "foo!!\n" ]
[main.func.expressions.2]
name = "return"
args = [ 0 ]
```
